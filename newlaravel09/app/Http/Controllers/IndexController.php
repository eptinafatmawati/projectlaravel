<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function dataTables()
    {
        return view('data-tables');
    }
}
