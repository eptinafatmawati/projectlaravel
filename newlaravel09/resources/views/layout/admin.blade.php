<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') - admin</title>
    <!-- Tambahkan stylesheet AdminLTE -->
    <link rel="stylesheet" href="{{ asset('css/adminlte.css') }}">
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Header -->
        @include('layouts.partials.header')
        <!-- Sidebar -->
        @include('layouts.partials.sidebar')

        <!-- Konten -->
        <div class="content-wrapper">
            <section class="content">
                @yield('content')
            </section>
        </div>

        <!-- Footer -->
        @include('layouts.partials.footer')
    </div>
    <!-- Tambahkan script AdminLTE -->
    <script src="{{ asset('js/adminlte.js') }}"></script>
</body>
</html>
