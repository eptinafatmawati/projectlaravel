
@extends('layouts.admin')

@section('title', 'Data Tables')

@section('content')
    <div class="content-header">
        <h1>Data Tables</h1>
    </div>
    
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Example Data Table</h3>
            </div>
            <div class="box-body">
                <!-- Tambahkan tabel data Anda di sini -->
            </div>
        </div>
    </section>
@endsection
