<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru </h1>
    <h2>Sign Up Form</h2>
    <form method="POST" action="/welcome">
    @csrf
    <div>
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" id="first_name">
    </div>
    <div>
    </select>
    <br>
        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" id="last_name">
    </div>
    <div>
        <label for="gender">Gender:</label>
        <div>
            <input type="radio" name="gender" id="gender_male" value="male"> 
            <label for="gender_male">Male</label>
        </div>
        <div>
            <input type="radio" name="gender" id="gender_female" value="female"> 
            <label for="gender_female">Female</label>
        </div>
        <div>
            <input type="radio" name="gender" id="gender_other" value="other"> 
            <label for="gender_other">Other</label>
        </div>
    </div>
    <div>
        <label for="nationality">Nationality: </label>
        </select>
        <br>
        <select name="nationality" id="nationality"> 
            <option value="indonesia">Indonesia</option>
            <option value="usa">USA</option>
            <option value="uk">UK</option>
        </select>
    </div>
    <div>
        <label>Language Spoken:</label>
        <div>
            <input type="checkbox" name="language_spoken[]" value="english"> English
        </div>
        <div>
            <input type="checkbox" name="language_spoken[]" value="spanish"> Spanish
        </div>
        <div>
            <input type="checkbox" name="language_spoken[]" value="french"> French
        </div>
    </div>
    <div>
        <label for="bio">Bio:</label>
        <textarea name="bio" id="bio"></textarea>
    </div>
    <div>
        <button type="submit">Sign Up</button>
    </div>
</form>



</body>
</html>
